package com.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeProjectInnoventesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeProjectInnoventesApplication.class, args);
	}

}
